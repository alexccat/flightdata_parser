﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections;

namespace FlightData_Parser
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        string inFile_ = string.Empty;
        string outFile_ = string.Empty;
        
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog_ = new OpenFileDialog();
            dialog_.Filter = "txt文件(*.txt)|*.txt";
            if (dialog_.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                inFile_ = dialog_.FileName;
                if (inFile_ != null)
                {
                    textBox1.Text = inFile_;

                    FileInfo fi = new FileInfo(inFile_);
                    labelFileName.Text = fi.Name;
                    labelFileEditTime.Text = fi.LastWriteTime.ToString();
                    labelFileSize.Text = ConvertUnit(fi.Length);

                    MessageBox.Show("请更改时间！", "提示", MessageBoxButtons.OK);
                    dateTimePicker1.Focus();
                }                
            }
        }

        private static string ConvertUnit(long size)
        {
            string FileSize = string.Empty;
            if (size > (1024 * 1024 * 1024))
                FileSize = string.Format("{0}GB", ((double)size / (1024 * 1024 * 1024)).ToString(".##"));
            else if (size > (1024 * 1024))
                FileSize = string.Format("{0}MB", ((double)size / (1024 * 1024)).ToString(".##"));
            else if (size > 1024)
                FileSize = string.Format("{0}KB", ((double)size / 1024).ToString(".##"));
            else if (size == 0)
                FileSize = "0 Byte";
            else
                FileSize = string.Format("{0}Byte", ((double)size / 1).ToString(".##"));

            return FileSize;
        }
        
  
        string dataNameStr_ = "飞行时间  GPS时间       北京时间         经度          纬度    海拔高度       地速          航迹角   GNSS东速  GNSS北速   GNSS天速      航向角    航向给定     俯仰角        俯仰给定      横滚角       横滚给定      俯仰角速率   横滚角速率    偏航角速率      纵向过载      横向过载     法向过载       真空速         表速        气压高度      升降速度     侧偏距     余油     耗油率    发送指令    响应指令";
        void analysisData(string inPath, string outPath)
        {
            string fileN_ = System.IO.Path.GetFileNameWithoutExtension(inPath);
            StreamReader sr = new StreamReader(inPath, Encoding.Default);
            String line;
            string svName_ = outPath + "\\"+ fileN_+".dat";
            FileStream fs = new FileStream(svName_, FileMode.Create);
            StreamWriter sw = new StreamWriter(fs);
            sw.WriteLine(dataNameStr_);

            double fTiem = 0d;

            DateTime gpsTime = dateTimePicker1.Value.AddHours(-8);            
            DateTime bjTime = dateTimePicker1.Value;

            Random random_ = new Random();
            double oilCoInit = random_.Next(52, 74) / 10;//油耗
            double oilFly_Init = random_.Next(169, 198) / 10;

            double oilQuan = 45.0d;//油量

            string oilCoStr_ = "";
            double oilFrame_ = 0d;

            List<double> barHlist = new List<double>();

            double sdis_ = 5d;//侧偏距 

            
            while ((line = sr.ReadLine()) != null)
            {
                string[] param_ = Regex.Split(line, @"\s+", RegexOptions.IgnoreCase);
                if (param_.Length == 1)
                    continue;
                
                double numO_ = myNextDouble(-0.2, 0.2);//生成随机数
                double addTiem_ = double.Parse(param_[50]);
                double sTime_ = addTiem_ / 1000d;

                gpsTime = gpsTime.AddMilliseconds(addTiem_);
                bjTime = bjTime.AddMilliseconds(addTiem_);
                if ((int.Parse(param_[22].ToString()) & 1) == 1)
                {
                    //飞行状态
                    fTiem += sTime_;

                    oilFly_Init += numO_;
                    if (oilCoInit < 16.9)
                    {
                        oilFly_Init = 16.9 + myNextDouble(0, 0.2);

                    }
                    else if (oilCoInit > 19.8)
                    {
                        oilFly_Init = 19.8 + myNextDouble(-0.2, 0);
                    }
                    oilCoStr_ = oilFly_Init.ToString("f1");

                    oilFrame_ = oilFly_Init / 3600 / 5;
                    oilQuan -= oilFrame_;

                    sdis_ += Convert.ToDouble(random_.Next(-5, 5));
                }
                else
                {                                 
                    oilCoInit += numO_;
                    if (oilCoInit < 5.2)
                    {
                        double te_ = myNextDouble(0, 0.2);
                        //if (te_ > 0.1)
                        //    Console.WriteLine(te_.ToString());
                        oilCoInit = 5.2 + te_;

                    }
                    else if (oilCoInit > 7.4)
                    {
                        double te_ = myNextDouble(-0.2, 0);
                        //if (te_ < -0.2)
                        //    Console.WriteLine(te_.ToString());
                        oilCoInit = 7.4 + te_;
                    }
                    oilCoStr_ = oilCoInit.ToString("f1");

                    oilFrame_ = oilCoInit / 3600 / 5;
                    oilQuan -= oilFrame_;
                }
                    
                double gSpeed_ = double.Parse(param_[9]) * 0.0003048 * 3600;//地速
                double trackAn_ = double.Parse(param_[5]) / 10;
                double eGnss_ =Math.Sin(trackAn_*(Math.PI/180))* gSpeed_;
                double nGnss_ = Math.Cos(trackAn_ * (Math.PI / 180))* gSpeed_;
                double perD_ = myNextDouble(-0.1,0.1);
                double tAirSpeed = gSpeed_+ gSpeed_ * perD_;//真空速
                double waSpeed = tAirSpeed - myNextDouble(0.05,0.1);//表速
                double barHeight = double.Parse(param_[28]) / -8 * 0.3048;//气压高度;
                barHlist.Add(barHeight);
                double verSpeed = 0d;//升降速度
                if (barHlist .Count> 5)
                {
                    verSpeed = barHeight - barHlist[barHlist.Count - 6];
                }

                double sGnss_ = double.Parse(param_[33]) * 0.3048;

                string sendCom_ = "";
                string resCom_ = "";
                switch(param_[20])
                {
                    case "1":
                        sendCom_ = "invalid";
                        break;
                    case "6":
                        sendCom_ = "climb";
                        break;
                    case "7":
                        sendCom_ = "waitclimb";
                        break;
                    case "15":
                        sendCom_ = "circuit";
                        break;
                    case "21":
                        sendCom_ = "flyto";
                        break;
                    case "32":
                        sendCom_ = "repeat";
                        break;
                    case "33":
                        sendCom_ = "wait";
                        break;
                    default:
                        sendCom_ = "=";
                        break;
                }
                resCom_ = sendCom_;

                string wrLine_ = string.Format("{0,-10}{1,-14}{2,-14}{3,-14}{4,-14}{5,-10}{6,-14}" +
                    "{7,-14}{8,-10}{9,-10}{10,-10}{11,-14}{12,-10}{13,-14}{14,-14}{15,-14}{16,-14}" +
                    "{17,-14}{18,-14}{19,-14}{20,-14}{21,-14}{22,-14}{23,-14}{24,-14}{25,-14}{26,-14}{27,-10}{28,-10}{29,-10}{30,-10}{31,-10}",                                                                                                  
                    fTiem.ToString("f1"),//飞行时间
                    gpsTime.ToString("HH:mm:ss"),//gps时间
                    bjTime.ToString("HH:mm:ss"),//北京时间
                    double.Parse(param_[24]).ToString("f6"),//经度
                    double.Parse(param_[25]).ToString("f6"),//纬度
                    param_[32],//海拔高度
                    gSpeed_.ToString("f6"),//地速
                    trackAn_.ToString("f6"),//航迹角
                    eGnss_.ToString("f1"),//gnss东速
                    nGnss_.ToString("f1"),//gnss北速
                    sGnss_.ToString("f1"),//gnss天速
                    (double.Parse(param_[35]) / 10).ToString("f6"),//航向角
                    param_[45],//航向给定 
                    (double.Parse(param_[0]) / 1024 / Math.PI * 180).ToString("f6"),//俯仰角
                    (double.Parse(param_[8]) / 1024 / Math.PI * 180).ToString("f6"),//俯仰给定
                    (double.Parse(param_[1]) / 1024 / Math.PI * 180).ToString("f6"),//横滚角
                    (double.Parse(param_[4]) / 1024 / Math.PI * 180).ToString("f6"),//横滚给定
                    (double.Parse(param_[42]) / 375).ToString("f6"),//俯仰角速率
                    (double.Parse(param_[43]) / 375).ToString("f6"),//横滚角速率
                    (double.Parse(param_[44]) / 375).ToString("f6"),//偏航角速率
                    (double.Parse(param_[34]) / 10.19 / 9.7926).ToString("f6"), //纵向过载
                    (double.Parse(param_[3]) / 10.19 / 9.7926).ToString("f6"), //横向过载
                    (double.Parse(param_[48]) / 10.19 / 9.7926).ToString("f6"),//法向过载
                    tAirSpeed.ToString("f6"),//真空速
                    waSpeed.ToString("f6"),//表速
                    barHeight.ToString("f6"),//气压高度
                    verSpeed.ToString("f6"),//升降速度
                    sdis_.ToString("f1"),//侧偏距
                    oilQuan.ToString("f1"),//余油
                    oilCoStr_,//油耗
                    resCom_,
                    resCom_
                    );
                sw.WriteLine(wrLine_);
               // Console.WriteLine(param_.Length.ToString());
            }
            sw.Close();
            MessageBox.Show("转换完成!", "提示", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }


        private void button2_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog dialog = new FolderBrowserDialog();
            dialog.Description = "请选择文件路径";
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                outFile_ = dialog.SelectedPath;
                textBox2.Text = outFile_;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(!string.IsNullOrEmpty(inFile_) && !string.IsNullOrEmpty(outFile_))
            {
                analysisData(inFile_, outFile_);
            }
            else if(string.IsNullOrEmpty(inFile_))
            {
                MessageBox.Show("请先选源文件！", "警告", MessageBoxButtons.OK);
                return;
            }
            else if(string.IsNullOrEmpty(outFile_))
            {
                MessageBox.Show("请先转换后保存路径！", "警告", MessageBoxButtons.OK);
                return;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            outFile_ = System.Environment.CurrentDirectory;
            textBox2.Text = outFile_;

        }


        double myNextDouble(double miniDouble, double maxiDouble)
        {
            Random rand = new Random();
            return rand.NextDouble() * (maxiDouble - miniDouble) + miniDouble;
        }
    }
}
